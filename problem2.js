
const fs = require("fs")
const path = require("path");
const { stringify } = require("querystring");

const lipsum = path.resolve(__dirname, "./data/lipsum.txt");

let cb = (err, data) => {
    if (err) {
        console.error(err);
    } else {
        console.log("readingfile");
        // console.log(data);
    }
};

function convertToUpperCase(lipsum, newfilepath) {
    fs.readFile(lipsum, "utf-8", function (err, data) {
        if (err) {
            console.error(err);
        } else {
            console.log("readingfile");

            let UpperCaseData = data.toString().toUpperCase()

            console.log(UpperCaseData);
            fs.writeFile(newfilepath, UpperCaseData, cb)


            fs.writeFileSync('/home/rajesh/Desktop/JS_Drill-6/filenames.txt', "uppercase.txt");

        }

    })
}

convertToUpperCase(lipsum, '/home/rajesh/Desktop/JS_Drill-6/files/uppercase.txt')


function convertToLowerCase(lipsum, newfilepath) {

    fs.readFile(lipsum, "utf-8", function (err, data) {
        if (err) {
            console.error(err);
        } else {
            console.log("readingfile");
            LowercaseData = data.toString().toLowerCase().split(".").join(".\n");
            //   LowercaseData = LowercaseData.split('.')
            console.log(LowercaseData);
            fs.writeFile(newfilepath, LowercaseData, cb)
            fs.writeFileSync('/home/rajesh/Desktop/JS_Drill-6/filenames.txt', "lowercase.txt");
        }
    })
}

convertToLowerCase(lipsum, '/home/rajesh/Desktop/JS_Drill-6/files/lowercase.txt')



function sortThedata(lipsum, newfilepath) {
    fs.readFile(lipsum, "utf-8", function (err, data) {
        if (err) {
            console.error(err);
        } else {
            console.log("readingfile..for sorting");
            let soretedData = data.toString().split(".").sort().join(".\n");
            //  soretedData = 
            //    console.log(soretedData);
            fs.writeFile(newfilepath, soretedData, cb)
             fs.writeFile('/home/rajesh/Desktop/JS_Drill-6/filenames.txt', "sorted.txt");

        }
    })
}

sortThedata('/home/rajesh/Desktop/JS_Drill-6/files/lowercase.txt', '/home/rajesh/Desktop/JS_Drill-6/files/sorted.txt');
